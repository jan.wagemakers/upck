package main

import (
	"bufio"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"time"
)

func warn(url string, status bool, cmd string) {
	updown := "down"
	if status {
		updown = "up"
	}
	cmd = strings.Replace(cmd, "$url", url, -1)
	cmd = strings.Replace(cmd, "$status", updown, -1)
	shell := exec.Command("bash", "-c", cmd)
	shell.Start()
}

func readcfg(file string) ([]string, string) {
	urls := []string{}
	cmd := ""
	cfg, err := os.Open(file)
	if err != nil {
		log.Fatal(err)
	}
	defer cfg.Close()

	scanner := bufio.NewScanner(cfg)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.HasPrefix(line, "http") {
			urls = append(urls, scanner.Text())
		}
		if strings.HasPrefix(line, "cmd=") {
			cmd = line[4:]
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return urls, cmd
}

func main() {
	// read upck.cfg :
	// - put url's to check in the urls slice
	// - put command to run when site is up/down in cmd
	urls, cmd := readcfg("upck.cfg")

	// urlc : counter : 0  = site is down --> send mail now
	//                : -1 = site is down and mail has been send
	//                : 3  = site is up
	//                : <3 = site is down, no mail send yet
	var upc int8 = 3
	urlc := make([]int8, len(urls))
	for index := range urlc {
		urlc[index] = upc
	}

	// loop forever
	for {
		for index, url := range urls {
			fmt.Print(url)

			// check url
			resp, err := http.Get(url)
			print(" --> ")
			if err != nil {
				// Site is down
				println("down")
				if urlc[index] >= 0 {
					urlc[index]--
				}
				if urlc[index] == 0 {
					warn(url, false, cmd)
				}
			} else {
				// Site is up
				println(resp.Status)
				resp.Body.Close()
				if urlc[index] <= 0 {
					warn(url, true, cmd)
				}
				urlc[index] = upc
			}
		}
		time.Sleep(1 * time.Minute)
	}
}
