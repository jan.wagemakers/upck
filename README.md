### upck

**upck** is a little program written in go to check if a website is up or down.
If a site is down or back up, it can send a warning (for example: by email).

When **upck** starts, it reads the file **upck.cfg**.

Syntax of **upck.cfg**:
```
# command to execute when site is down or back up
# cmd = command
# $url = replaced with url of site
# $status = replaced with up or down
cmd=echo "$url is $status." | mutt -s "$url is $status." jan.wagemakers@gmail.com

# url's to check : should start with http
https://www.janwagemakers.be
http://janw.mooo.com:18452
http://janw.mooo.com:24388
http://janwagemakers.sdfeu.org/
```

Source is available at [gitlab](https://gitlab.com/jan.wagemakers/upck).
